<!DOCTYPE html>
<html lang="en">

@include('component/head')

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

@include('component/sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

@include('component/topbar')

                <!-- Begin Page Content -->
                <div class="container-fluid">

@yield('content')
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

@include('component/footer')

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

@include('component/button-top')

@include('component/logout-modal')

@include('component/js')

</body>

</html>