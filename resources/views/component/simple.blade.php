<!DOCTYPE html>
<html lang="en">

@include('component/head')

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">


@yield('content')
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->


@include('component/js')

</body>

</html>