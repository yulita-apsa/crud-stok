@extends('component.index')
@section('title', 'Stok Barang')
    

@section('content')

     <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Welcome to Stok Barang</h1>



    @if (Session::has('success'))
        <p class="alert alert-warning">{{Session::get('success')}}</p>
    @endif

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">

    {{--  Searchbar  --}}
    <form action="{{url('stok')}}" method="get" class="float-right">
        <input type="text" name="keyword" class="d-inline" value="{{$keyword}}"/>
        <button type="submit">
            <i class="fas fa-search"></i>
        </button>
    </form>
    <!-- Button trigger modal -->
    <a href="{{url('stok/create')}}" type="button" class="btn btn-secondary ">
      <i class="fas fa-plus"></i>
    </a>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table
        class="table table-bordered"
        id="dataTable"
        width="100%"
        cellspacing="0"
      >

        @csrf
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Tanggal Produksi</th>
            <th>Kadaluarsa Produk</th>
            <th>Made In</th>
            <th>Action</th>
          </tr>
        </thead>
        @foreach ($stok as $barang)
        <tr>
            <td>{{$barang->id}}</td>
            <td>{{$barang->name}}</td>
            <td>{{$barang->jumlah}}</td>
            <td>{{$barang->produksi}}</td>
            <td>{{$barang->kadaluarsa}}</td>
            <td>{{$barang->made}}</td>
            <td>
                <a href="{{url('stok/'.$barang->id.'/edit')}}" class="btn btn-primary btn-sm button-update">
                  <i class="fas fa-edit"></i></a>
            
                <form action="{{url('stok/'.$barang->id)}}" method="POST" class="d-inline">
                    @csrf
                    <input type="hidden" name=_method value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm">
                  <i class="fas fa-trash"></i></button>
                </form>
            </td>
        </tr>

        
        @endforeach
        </div>
            <div class="d-flex justify-content-end">
            {{$stok->links()}}
        </div>
      </table>
      
    </div>
  </div>
  


@endsection
